#include "audiomodule.h"
#include <array>
#include <iostream>
#include "WinDebugStream.h"

VstIntPtr VSTCALLBACK HostCallback(AEffect* effect, VstInt32 opcode, VstInt32 index, VstIntPtr value, void* ptr, float opt)
{
	char buf[256];
	// Extremely minimal, some plugins are likely going to blow up when returning 0 regardless of what parameters were passed in
	IAudioModule* host = nullptr;
	if (effect!=nullptr)
		host=(IAudioModule*)effect->resvd1;
	if (opcode == audioMasterVersion)
	{
		//AsyncExecutor::instance()->add_task([]() { OutputDebugStringA("plugin asked vst version\n"); });
		return kVstVersion;
	}
	if (opcode == audioMasterAutomate)
	{
		if (host != nullptr)
		{
			host->on_parameter_changed.notify(index, opt);
		}
	}
	return 0;
}


VSTPlugin::VSTPlugin(std::string plugfn) :
	m_input_matrix(64,64) , m_output_matrix(64,64)
{
	m_pluglib = DynamicLibrary(plugfn);
	if (m_pluglib.is_valid()==true)
	{
		std::cout << "Plugin dll loaded!\n";
		PluginEntryProc entryproc = m_pluglib.resolve<PluginEntryProc>("VSTPluginMain");
		// Some REALLY old plugins export main instead of VSTPluginMain...
		if (entryproc == nullptr)
			entryproc = m_pluglib.resolve<PluginEntryProc>("main");
		if (entryproc != nullptr)
		{
			std::cout << "Plugin main function loaded!\n";
			m_effect = entryproc(HostCallback);
			if (m_effect != nullptr)
			{
				std::cout << "PLUGIN LOADED!!!\n";
				m_effect->dispatcher(m_effect, effOpen, 0, 0, 0, 0);
				/*
				std::cout << "Plugin has " << m_effect->numParams << " parameters \n";
				for (int i = 0; i < m_effect->numParams; ++i)
				{
				std::cout << "\t" << i << " : " << get_parameter_name(i) << "\n";
				}
				*/
				std::cout << "Plugin has " << m_effect->numInputs << " inputs\n";
				std::cout << "Plugin has " << m_effect->numOutputs << " outputs\n";
				for (int i = 0; i < m_effect->numInputs; ++i)
					m_input_matrix.set_connection(i, i, true);
				for (int i = 0; i < m_effect->numOutputs; ++i)
					m_output_matrix.set_connection(i, i, true);
				m_plugfn = plugfn;
				m_effect->resvd1 = (VstIntPtr)this;
			}
		}
		else std::cout << "plugin main function not loaded\n";
	}

}

inline VSTPlugin::~VSTPlugin()
{
	if (m_effect != nullptr)
	{
		m_effect->dispatcher(m_effect, effClose, 0, 0, 0, 0);
	}
	
}

inline void VSTPlugin::prepare_to_process(int blocksize, int samplerate)
{
	if (m_effect == nullptr)
		return;
	m_effect->dispatcher(m_effect, effMainsChanged, 0, 0, 0, 0);
	m_effect->dispatcher(m_effect, effSetBlockSize, 0, blocksize, 0, 0);
	m_effect->dispatcher(m_effect, effSetSampleRate, 0, 0, 0, samplerate);
	m_effect->dispatcher(m_effect, effMainsChanged, 0, 1, 0, 0);
	m_in_buffers.resize(m_effect->numInputs*blocksize);
	m_out_buffers.resize(m_effect->numOutputs*blocksize);
	m_matrix_buf.resize(64*blocksize);
	m_is_initialized = true;
}

inline void VSTPlugin::release_resources() 
{
	if (m_is_initialized == true)
	{
		m_effect->dispatcher(m_effect, effStopProcess, 0, 0, 0, 0);
	}
}

inline void VSTPlugin::process_audio(double * buf, int nframes, int inchans, int outchans)
{
	if (m_effect == nullptr)
	{
		return;
	}
	
	int plugnumins = m_effect->numInputs;
	int plugnumouts = m_effect->numOutputs;
	m_input_matrix.process(buf, inchans, m_matrix_buf.data(), plugnumins, nframes, true);
	// Make split buffers for the plugin and convert double samples to float since
	// for example Juce based plugins don't support 64 bit floating point I/O
	std::array<float*, 64> inbufs; // max 64 chans for now...
	
	for (int i = 0; i < plugnumins; ++i)
		inbufs[i] = &m_in_buffers[m_in_buffers.size() / plugnumins*i];
	std::array<float*, 64> outbufs;
	for (int i = 0; i < plugnumouts; ++i)
		outbufs[i] = &m_out_buffers[m_out_buffers.size() / plugnumouts*i];
	for (int i = 0; i < plugnumins; ++i)
	{
		for (int j = 0; j < nframes; ++j)
		{
			inbufs[i][j] = m_matrix_buf[j*plugnumins + i];
		}
	}
	// Whoo, do the plugin processing
	m_effect->processReplacing(m_effect, inbufs.data(), outbufs.data(), nframes);
	// Convert back to interleaved double buffer
	for (int i = 0; i < plugnumouts; ++i)
	{
		for (int j = 0; j < nframes; ++j)
		{
			m_matrix_buf[j * plugnumouts + i] = outbufs[i][j];
		}
	}
	// output matrixing
	m_output_matrix.process(m_matrix_buf.data(), plugnumouts, buf, outchans, nframes, true);
}

inline int VSTPlugin::get_num_parameters()
{
	if (m_effect == nullptr)
		return 0;
	return m_effect->numParams;
}

inline std::string VSTPlugin::get_parameter_name(int index)
{
	if (m_effect == nullptr)
		return "Invalid";
	char paramName[256] = { 0 };
	m_effect->dispatcher(m_effect, effGetParamName, index, 0, paramName, 0);
	return paramName;
}

inline void VSTPlugin::set_parameter(int index, double value)
{
	if (m_effect == nullptr)
		return;
	m_effect->setParameter(m_effect, index, value);
}

inline double VSTPlugin::get_parameter(int index)
{
	if (m_effect == nullptr)
		return 0.0;
	return m_effect->getParameter(m_effect, index);
}

inline bool VSTPlugin::is_valid() const
{
	return m_effect != nullptr;
}

routing_matrix * VSTPlugin::get_routing_matrix(int which)
{
	if (which == 0)
		return &m_input_matrix;
	return &m_output_matrix;
}

int VSTPlugin::get_num_inputs() const
{
	if (m_effect==nullptr)
		return 0;
	return m_effect->numInputs;
}

int VSTPlugin::get_num_outputs() const
{
	if (m_effect==nullptr)
		return 0;
	return m_effect->numOutputs;
}

std::string VSTPlugin::get_type_name() const
{
	return m_plugfn;
}

std::string VSTPlugin::get_instance_name() const
{
	if (m_effect == nullptr)
		return std::string();
	if (m_instancename.empty() == false)
		return m_instancename;
	char effectName[256] = { 0 };
	m_effect->dispatcher(m_effect, effGetEffectName, 0, 0, effectName, 0);
	return std::string(effectName);
}

void VSTPlugin::set_instance_name(std::string name)
{
	m_instancename = name;
}

IAudioModule * VSTPlugin::duplicate()
{
	VSTPlugin* dup = nullptr;
	try
	{
		dup = new VSTPlugin(m_plugfn);
		dup->m_input_matrix = m_input_matrix;
		dup->m_output_matrix = m_output_matrix;
		for (int i = 0; i < get_num_parameters(); ++i)
		{
			dup->set_parameter(i, get_parameter(i));
		}
	}
	catch (...)
	{
		// Whoops, cool handling...
		std::cout << "Could not create duplicate of " << m_plugfn << "\n";
	}
	return dup;
}

void VSTPlugin::open_editor(WindowHandle parent)
{
	if (m_effect == nullptr)
		return;
	m_effect->dispatcher(m_effect, effEditOpen, 0, 0, parent, 0);
}

void VSTPlugin::close_editor()
{
	if (m_effect == nullptr)
		return;
	m_effect->dispatcher(m_effect, effEditClose, 0, 0, 0, 0);
}

MultiChannelAdapter::MultiChannelAdapter(std::unique_ptr<IAudioModule>& createfrom, int numinswanted, int numoutswanted)
{
	if (numoutswanted <= createfrom->get_num_outputs())
	{
		std::cout << "multichannel adapter not needed. but proceeding anyway. sigh.\n";
	}
	m_num_inputs = numinswanted;
	m_num_outputs = numoutswanted;
	int plugoutchans = createfrom->get_num_outputs();
	int numneededplugins = (numoutswanted / plugoutchans) + (numoutswanted % plugoutchans);
	std::cout << "creating " << numneededplugins << " copies of " << createfrom->get_instance_name() << "\n";
	m_modules.push_back(std::move(createfrom));
	for (int i = 1; i<numneededplugins; ++i)
	{
		IAudioModule* dup = m_modules.front()->duplicate();
		
		m_modules.push_back(std::unique_ptr<IAudioModule>(dup));
	}
	m_paramgroupfunc = [this](int modindex, int modcount, int parindex, double val)
	{
		return val;
	};
	for (int i = 0; i < m_modules.size(); ++i)
		m_modules[i]->on_parameter_changed.add_listener ( [this, i](int paramindex, double paramvalue) 
		{
			if (paramindex >= 0)
			{
				cwindbg() << "param " << paramindex << " of module instance " << i << " changed to " << paramvalue << "\n";
				for (int j = 0; j < m_modules.size(); ++j)
				{
					if (j != i)
					{
						double transformed_val = m_paramgroupfunc(j, m_modules.size(), paramindex, paramvalue);
						m_modules[j]->set_parameter(paramindex, paramvalue);
					}
				}
			}
			else
			{
				cwindbg() << "plugin notified of special param " << paramindex << "\n";
			}
		});
}

void MultiChannelAdapter::prepare_to_process(int blocksize, int samplerate)
{
	m_tempbuf.resize(blocksize * 64);
	for (auto& e : m_modules)
		e->prepare_to_process(blocksize, samplerate);
}

void MultiChannelAdapter::release_resources()
{
	for (auto& e : m_modules)
		e->release_resources();
}

void MultiChannelAdapter::process_audio(double * buf, int nframes, int inchans, int outchans)
{
	if (inchans != m_num_inputs || outchans != m_num_outputs)
		return;
	int nativeinchancount = m_modules.front()->get_num_inputs();
	int nativeoutchancount = m_modules.front()->get_num_outputs();
	for (int i = 0; i < m_modules.size(); ++i)
	{
		int chanoffs = nativeinchancount*i;
		for (int j = 0; j < nativeinchancount; ++j)
		{
			for (int k = 0; k < nframes; ++k)
			{
				m_tempbuf[k * nativeinchancount + j] = buf[k * inchans + (j+chanoffs)];
			}
		}
		m_modules[i]->process_audio(m_tempbuf.data(), nframes, nativeinchancount, nativeoutchancount);
		for (int j = 0; j < nativeoutchancount; ++j)
		{
			int outchanoffs = nativeoutchancount*i;
			for (int k = 0; k < nframes; ++k)
			{
				buf[k * outchans + (j+outchanoffs)] = m_tempbuf[k*nativeoutchancount + j];
			}
		}
	}
}

int MultiChannelAdapter::get_num_parameters()
{
	return m_modules.front()->get_num_parameters();
}

std::string MultiChannelAdapter::get_parameter_name(int index)
{
	return m_modules.front()->get_parameter_name(index);
}

void MultiChannelAdapter::set_parameter(int index, double value)
{
	for (auto& e : m_modules)
		e->set_parameter(index, value);
}

double MultiChannelAdapter::get_parameter(int index)
{
	return m_modules.front()->get_parameter(index);
}

bool MultiChannelAdapter::is_valid() const
{
	return m_modules.front()->is_valid();
}

routing_matrix * MultiChannelAdapter::get_routing_matrix(int which)
{
	if (which == 0)
		return &m_input_matrix;
	return &m_output_matrix;
}

int MultiChannelAdapter::get_num_inputs() const
{
	return m_num_inputs;
}

int MultiChannelAdapter::get_num_outputs() const
{
	return m_num_outputs;
}

std::string MultiChannelAdapter::get_type_name() const
{
	return "MultiChannel Adapter";
}

std::string MultiChannelAdapter::get_instance_name() const
{
	return m_modules.front()->get_instance_name();
}

void MultiChannelAdapter::set_instance_name(std::string name)
{
}

IAudioModule * MultiChannelAdapter::duplicate()
{
	return nullptr;
}

void MultiChannelAdapter::open_editor(WindowHandle parent)
{
	m_modules.front()->open_editor(parent);
}

void MultiChannelAdapter::close_editor()
{
	m_modules.front()->close_editor();
}
