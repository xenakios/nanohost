#pragma once

#include <string>
#include <vector>
#include <memory>
#include <functional>
#include <iostream>
#include "aeffectx.h"
#include "routingmatrix.h"
#include "observer.h"
#ifdef WIN32
#include "Windows.h"
using WindowHandle = HWND;
#else
#include <dlfcn.h>"
using WindowHandle = void*;
#endif

typedef AEffect* (*PluginEntryProc) (audioMasterCallback audioMaster);

// This is the function the plugin calls at various times, ie plugin->host communication
VstIntPtr VSTCALLBACK HostCallback(AEffect* effect, VstInt32 opcode, VstInt32 index, VstIntPtr value, void* ptr, float opt);

// Base class to inherit from, if we want to support some host built-in audio processing too...Or other plugin formats
class IAudioModule
{
public:
	virtual ~IAudioModule() {}
	// should be called before starting to process audio
	virtual void prepare_to_process(int blocksize, int samplerate) = 0;
	// should be called after all audio processed
	virtual void release_resources() = 0;
	// obvious
	virtual void process_audio(double* buf, int nframes, int inchans = 2, int outchans = 2) = 0;
	virtual int get_num_parameters() = 0;
	virtual std::string get_parameter_name(int index) = 0;
	virtual void set_parameter(int index, double value) = 0;
	virtual double get_parameter(int index) = 0;
	BroadCaster<int, double> on_parameter_changed;
	// is the module ready to do anything interesting
	virtual bool is_valid() const = 0;
	virtual routing_matrix* get_routing_matrix(int which) = 0;
	virtual int get_num_inputs() const = 0;
	virtual int get_num_outputs() const = 0;
	// Shared name to be used for host project file saving etc
	virtual std::string get_type_name() const = 0;
	// Instance specific name
	virtual std::string get_instance_name() const = 0;
	virtual void set_instance_name(std::string name) = 0;
	virtual IAudioModule* duplicate() = 0;
	virtual void open_editor(WindowHandle parent_hwnd) {}
	virtual void close_editor() {}
};

class DynamicLibrary
{
public:
	DynamicLibrary() {}
	DynamicLibrary(std::string fn)
	{
#ifdef WIN32
		auto customdelete = [](void* ptr) 
		{ 
			HMODULE hm = (HMODULE)ptr;
			if (hm!=nullptr) 
				FreeLibrary(hm); 
		};
		HMODULE temp = LoadLibraryA(fn.c_str());
		m_lib = std::shared_ptr<void>(temp, customdelete);
#else
		auto customdelete = [](void* ptr)
		{
			if (ptr != nullptr)
				dlclose(ptr);
		};
		void* temp = dlopen(fn.c_str(), RTLD_LOCAL | RTLD_NOW);
		m_lib = std::shared_ptr<void>(temp, customdelete);
		
#endif
	}
	template<typename F>
	F resolve(const char* funcname)
	{
#ifdef WIN32
		return reinterpret_cast<F>(GetProcAddress((HMODULE)m_lib.get(), funcname));
#else
		return reinterpret_cast<F>(dlsym(m_lib.get(), funcname));
#endif
	}
	bool is_valid() const
	{
		return m_lib != nullptr;
	}
private:
	std::shared_ptr<void> m_lib;
};

class VSTPlugin : public IAudioModule
{
public:
	VSTPlugin(std::string plugfn);
	~VSTPlugin();
	void prepare_to_process(int blocksize, int samplerate) override;
	void release_resources() override;
	void process_audio(double* buf, int nframes, int inchans = 2, int outchans = 2) override;
	int get_num_parameters() override;
	std::string get_parameter_name(int index) override;
	void set_parameter(int index, double value) override;
	double get_parameter(int index) override;
	bool is_valid() const override;
	routing_matrix* get_routing_matrix(int which) override;
	int get_num_inputs() const override;
	int get_num_outputs() const override;
	std::string get_type_name() const override;
	std::string get_instance_name() const override;
	void set_instance_name(std::string name) override;
	IAudioModule* duplicate() override;
	void open_editor(WindowHandle parent) override;
	void close_editor() override;
private:

	DynamicLibrary m_pluglib;

	AEffect* m_effect = nullptr;
	std::vector<float> m_in_buffers;
	std::vector<float> m_out_buffers;
	std::vector<double> m_matrix_buf;
	routing_matrix m_input_matrix;
	routing_matrix m_output_matrix;
	bool m_is_initialized = false;
	std::string m_plugfn;
	std::string m_instancename;
};

class MultiChannelAdapter : public IAudioModule
{
public:
	MultiChannelAdapter(std::unique_ptr<IAudioModule>& createfrom, int numins, int numouts);
	// Inherited via IAudioModule
	virtual void prepare_to_process(int blocksize, int samplerate) override;
	virtual void release_resources() override;
	virtual void process_audio(double * buf, int nframes, int inchans = 2, int outchans = 2) override;
	virtual int get_num_parameters() override;
	virtual std::string get_parameter_name(int index) override;
	virtual void set_parameter(int index, double value) override;
	virtual double get_parameter(int index) override;
	virtual bool is_valid() const override;
	virtual routing_matrix * get_routing_matrix(int which) override;
	virtual int get_num_inputs() const override;
	virtual int get_num_outputs() const override;
	virtual std::string get_type_name() const override;
	virtual std::string get_instance_name() const override;
	virtual void set_instance_name(std::string name) override;
	virtual IAudioModule * duplicate() override;
	void open_editor(WindowHandle parent) override;
	void close_editor() override;
private:
	std::vector<std::unique_ptr<IAudioModule>> m_modules;
	std::vector<double> m_tempbuf;
	routing_matrix m_input_matrix;
	routing_matrix m_output_matrix;
	int m_num_inputs = 0;
	int m_num_outputs = 0;
	std::function<double(int, int, int, double)> m_paramgroupfunc;
};
