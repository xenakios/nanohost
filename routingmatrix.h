#ifndef ROUTINGMATRIX
#define ROUTINGMATRIX

#include <vector>
#include <algorithm>

class routing_matrix
{
public:
    routing_matrix() {}
    routing_matrix(int numins, int numouts)
        : m_max_num_ins(numins), m_max_num_outs(numouts)
    {
        m_matrix.resize(m_max_num_ins*m_max_num_outs);
    }
    void init_default_routings(int inchans, int outchans)
    {
        std::fill(m_matrix.begin(),m_matrix.end(),0.0);
        if (inchans>m_max_num_ins || outchans>m_max_num_outs)
        {
            //qDebug() << "routing matrix error";
            return;
        }
        m_uses_default=true;
        if (inchans==1 && outchans>1)
        {
            // 1.0 1.0 0.0 0.0 0.0 0.0 0.0 0.0
            // 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0
            m_matrix[0]=1.0;
            m_matrix[1]=1.0;
        }
        if (inchans==8 && outchans==4)
        {
            // 1 0 0 0 0 0 0 0
            // 0 1 0 0 0 0 0 0
            // 0 0 1 0 0 0 0 0
            // 0 0 0 1 0 0 0 0
            // 1 0 0 0 0 0 0 0
            // 0 1 0 0 0 0 0 0
            // 0 0 1 0 0 0 0 0
            // 0 0 0 1 0 0 0 0
            m_matrix[0]=1.0;
            m_matrix[9]=1.0;
            m_matrix[18]=1.0;
            m_matrix[27]=1.0;
            m_matrix[32]=1.0;
            m_matrix[41]=1.0;
            m_matrix[50]=1.0;
            m_matrix[59]=1.0;
            return;
        }
        if (inchans>1 && outchans>1)
        {
            // 1.0 0.0 0.0 0.0
            // 0.0 1.0 0.0 0.0
            // 0.0 0.0 1.0 0.0
            // 0.0 0.0 0.0 1.0
            for (int i=0;i<inchans;++i)
            {
                m_matrix[i*m_max_num_outs+i]=1.0;
                // 0 0
                // 1 5

            }
        }
    }
    void process(double* inbuf,
                 int numins,
                 double* outbuf,
                 int numouts,
                 int nframes,
                 bool prezero_output)
    {
        if (prezero_output==true)
            for (int i=0;i<nframes*numouts;++i)
                outbuf[i]=0.0;
        for (int i=0;i<nframes;++i)
        {
            for (int j=0;j<numins;++j)
            {
                for (int k=0;k<numouts;++k)
                {
                    double gain=m_matrix[j*m_max_num_outs+k];
                    outbuf[i*numouts+k]+=inbuf[i*numins+j]*gain;
                }
            }
        }
    }
    std::vector<double>& getMatrixData()
    {
        return m_matrix;
    }
    void set_connection(int input_chan, int output_chan, bool connected)
    {
        double gain=0.0;
        if (connected==true)
            gain=1.0;
        m_matrix[input_chan*m_max_num_outs+output_chan]=gain;
    }
    void clear_all()
    {
        std::fill(m_matrix.begin(),m_matrix.end(),0.0);
    }

    int getMaxNumInputs() const { return m_max_num_ins; }
    int getMaxNumOutputs() const { return m_max_num_outs; }
    bool m_was_manually_edited=false;
private:
    std::vector<double> m_matrix;
    int m_max_num_ins=0;
    int m_max_num_outs=0;
    bool m_uses_default=false;
};

#endif // ROUTINGMATRIX

