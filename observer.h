#pragma once

#include <vector>
#include <functional>
#ifdef WIN32
#include <Windows.h>
#else
#include "dispatch/dispatch.h"
#endif
struct empty_t {};

class AsyncExecutor
{
public:
	AsyncExecutor();
	static AsyncExecutor* instance();
	static void finish();
	void add_task(std::function<void(void)>&& f);
	void run_tasks();
	void run_oldest_task();
private:
	
	std::vector < std::function<void(void)>> m_async_tasks;
#ifdef WIN32
    UINT_PTR m_timer_id = 0;
#else
    void* m_timer_id = 0;
#endif
};

template<typename... Ts>
class BroadCaster
{
public:
	using func_t = std::function<void(Ts...)>;
	BroadCaster() {}
	int add_listener(func_t f)
	{
		static int counter = 0; // not thread safe...oh well...
		int temp = counter;
		m_listeners.push_back(std::make_pair(f,counter));
		++counter;
		return temp;
	}
	void remove_listener(int id)
	{
		for (int i = 0; i < m_listeners.size();++i)
			if (m_listeners[i].second == id)
			{
				m_listeners.erase(m_listeners.begin() + i);
				break;
			}
	}
	void notify(Ts... x)
	{
		for (auto& e : m_listeners)
			e.first(x...);
	}
	void notify_async(Ts... x)
	{
		for (auto& e : m_listeners)
			AsyncExecutor::instance()->add_task([x..., e](){ e.first(x...); });
	}
	
private:
	std::vector<std::pair<func_t,int>> m_listeners;
};
