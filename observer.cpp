#include "observer.h"
#include <mutex>
#include <cstdlib>
AsyncExecutor* g_async_executor = nullptr;

std::mutex g_async_ex_mutex;
#ifdef WIN32
VOID CALLBACK AsyncTasksTimerProc(
	_In_ HWND     hwnd,
	_In_ UINT     uMsg,
	_In_ UINT_PTR idEvent,
	_In_ DWORD    dwTime
	)
{
	//OutputDebugStringA("async executor timer...\n");
	if (g_async_executor != nullptr)
		g_async_executor->run_oldest_task();
}
#endif

AsyncExecutor::AsyncExecutor()
{
#ifdef WIN32
    m_timer_id = SetTimer(0, 6666, 1, AsyncTasksTimerProc);
#endif
	m_async_tasks.reserve(256);
}

void async_exec_exithandler()
{
	//OutputDebugStringA("exit handler!\n");
	AsyncExecutor::finish();
}

AsyncExecutor * AsyncExecutor::instance()
{
	if (g_async_executor == nullptr)
	{
		g_async_executor = new AsyncExecutor;
		std::atexit(async_exec_exithandler);
	}
	return g_async_executor;
}

void AsyncExecutor::finish()
{
	if (g_async_executor != nullptr)
	{
#ifdef WIN32
        KillTimer(NULL, g_async_executor->m_timer_id);
#endif
		g_async_executor->run_tasks();
	}
}

void AsyncExecutor::add_task(std::function<void(void)>&& f)
{
#ifdef WIN32
    if (f)
	{
		std::lock_guard<std::mutex> locker(g_async_ex_mutex);
        m_async_tasks.push_back(std::move(f));
	}
#else
    // can't really avoid a heap allocation here but at least moving the function might help a little...
    using func_t=std::function<void(void)>;
    func_t* heapcopy=new func_t(std::move(f));
    dispatch_async_f(dispatch_get_main_queue (), (void*)heapcopy, [](void *ctxptr)
                     {
                         func_t* fptr=static_cast<func_t*>(ctxptr);
                         (*fptr)();
                         delete fptr;
                     });
#endif
}

void AsyncExecutor::run_tasks()
{
	// lock for the whole duration since this at least for now
	// is run only at the end of the program
	std::lock_guard<std::mutex> locker(g_async_ex_mutex);
	for (auto& e : m_async_tasks)
		e();
	m_async_tasks.clear();
}

void AsyncExecutor::run_oldest_task()
{
	// This is completely horrible and probably not correct, but shall suffice for the moment
	g_async_ex_mutex.lock();
	auto size = m_async_tasks.size();
	g_async_ex_mutex.unlock();
	if (size == 0)
		return;
	g_async_ex_mutex.lock();
	auto f = std::move(m_async_tasks.front());
	g_async_ex_mutex.unlock();
	
	// execute outside mutex to actually get some async
	f();
	
	g_async_ex_mutex.lock();
	m_async_tasks.erase(m_async_tasks.begin());
	g_async_ex_mutex.unlock();
}
