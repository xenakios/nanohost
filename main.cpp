#ifdef WIN32
#include "Windows.h"
#include "resource.h"
#endif
#include <string>
#include "aeffectx.h"
#include <iostream>
#include <vector>
#include <array>
#include <memory>
#include "sndfile.h"
//#include "picojson.h"
#include "audiomodule.h"

#include <map>
#ifdef WIN32
// Damned Windows.h...
#undef min
#undef max

std::map<WindowHandle, IAudioModule*> g_blahmap;

void close_plugin_gui(WindowHandle h)
{
	if (g_blahmap.count(h) > 0)
	{
		g_blahmap[h]->close_editor();
	}
}

INT_PTR CALLBACK MyDialogProc(
	_In_ HWND   hwndDlg,
	_In_ UINT   uMsg,
	_In_ WPARAM wParam,
	_In_ LPARAM lParam
	)
{
	char buf[100];
	if (uMsg == WM_INITDIALOG)
	{
		IAudioModule* mo = (IAudioModule*)lParam;
		g_blahmap[hwndDlg] = mo;
		mo->open_editor(hwndDlg);
		//std::string* str = (std::string*)lParam;
		//OutputDebugStringA(str->c_str());
	}
	if (uMsg == WM_CLOSE)
	{
		close_plugin_gui(hwndDlg);
		EndDialog(hwndDlg, 0);
	}
	if (uMsg == WM_COMMAND)
	{
		if (LOWORD(wParam) == IDCANCEL)
		{
			close_plugin_gui(hwndDlg);
			EndDialog(hwndDlg, 0);
			return 0;
		}
		if (LOWORD(wParam) == IDOK)
		{
			close_plugin_gui(hwndDlg);
			EndDialog(hwndDlg, 666);
			return 0;
		}
	}
	return 0;
}
#endif

//#define TESTPLUGINGUI

#ifdef TESTPLUGINGUI
int WinMain(HINSTANCE,HINSTANCE,LPSTR,int)
#else
int main(int argc, char** argv)
#endif
{
	const int bufsize = 512;
	std::vector<std::string> plugs_to_test;
	plugs_to_test.push_back("C:/Program Files/VSTPlugins/GRM Shuffling Stereo.dll");
	//plugs_to_test.push_back("C:/Net_Downloads/14082015/reafir_standalone.dll");
#ifdef WIN32
    //plugs_to_test.push_back("C:/Program Files/VST_Plugins_x64/ValhallaRoom_x64.dll");
#else
    //plugs_to_test.push_back("/Library/Audio/Plug-Ins/VST/ValhallaRoom_x64.vst/Contents/MacOS/ValhallaRoom_x64");
    plugs_to_test.push_back("/Library/Audio/Plug-Ins/VST/mda Image.vst/Contents/MacOS/mda Image");
#endif
	//plugs_to_test.push_back("C:/Program Files/VST_Plugins_x64/mda-vst-bin-win-64-2010-02-14/mdaDubDelay.dll");
	for (int i = 0; i < plugs_to_test.size(); ++i)
	{
		std::cout << plugs_to_test[i] << "\n";
		std::unique_ptr<IAudioModule> pluga = std::make_unique<VSTPlugin>(plugs_to_test[i]);
        if (pluga->is_valid()==true)
            std::cout << "plug was loaded\n";
        else std::cout << "plug was NOT loaded\n";
        return 0;
		std::unique_ptr<MultiChannelAdapter> multiplug = std::make_unique<MultiChannelAdapter>(pluga, 8, 8);

#ifdef WIN32
        auto r = DialogBoxParamA(GetModuleHandleW(NULL), MAKEINTRESOURCEA(101), NULL, MyDialogProc, (LPARAM)multiplug.get());
		//return 0;
		if (r == 0)
		{
			OutputDebugStringA("cancelled dialog\n");
			return 0;
		}
		if (r == 666)
			OutputDebugStringA("rendering...\n");
		//
#endif
		//break;
		SF_INFO in_info = { 0 };
		SNDFILE* infile = sf_open("C:/MusicAudio/sourcesamples/multichantest/count_8ch.wav", SFM_READ, &in_info);
		SF_INFO out_info = { 0 };
		out_info.channels = 8;
		out_info.format = SF_FORMAT_WAV | SF_FORMAT_FLOAT;
		out_info.samplerate = 44100;
		std::string outname = "C:/MusicAudio/nanohosttests/nanohost" + std::to_string(i) + ".wav";
		SNDFILE* outfile = sf_open(outname.c_str(), SFM_WRITE, &out_info);
		if (infile != nullptr && outfile != nullptr)
		{
			/*
			plug->get_routing_matrix(0)->clear_all();
			plug->get_routing_matrix(0)->set_connection(1, 0, true);
			plug->get_routing_matrix(0)->set_connection(0, 1, true);
			plug->get_routing_matrix(1)->clear_all();
			plug->get_routing_matrix(1)->set_connection(0, 1, true);
			plug->get_routing_matrix(1)->set_connection(1, 0, true);
			plug->get_routing_matrix(1)->set_connection(0, 3, true);
			*/
			multiplug->prepare_to_process(bufsize, 44100);
			//pluga->set_parameter(0, 0.5);
			std::vector<double> buf(bufsize * 64);
			int64_t filecounter = 0;
			while (filecounter < in_info.frames)
			{
				sf_readf_double(infile, buf.data(), bufsize);
				multiplug->process_audio(buf.data(), bufsize, in_info.channels, out_info.channels);
				int framestowrite = std::min((int64_t)bufsize, in_info.frames-filecounter);
				sf_writef_double(outfile, buf.data(), framestowrite);
				filecounter += bufsize;
			}
			//OutputDebugStringA("finished");
			sf_close(infile);
			sf_close(outfile);
			multiplug->release_resources();
		}
	}
	
}